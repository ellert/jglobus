Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: JGlobus
Source: https://github.com/jglobus/JGlobus/archive/JGlobus-Release-2.1.0.tar.gz

Files: *
Copyright: 1999-2014 University of Chicago
License: Apache-2.0

Files: ssl-proxies/src/main/java/org/globus/tools/GridCertRequest.java
Copyright: 2003 National Research Council of Canada
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to 
 deal in the Software without restriction, including without limitation the 
 rights to use, copy, modify, merge, publish, distribute, and/or sell copies 
 of the Software, and to permit persons to whom the Software is furnished to
 do so, subject to the following conditions:
 .
 The above copyright notice(s) and this licence appear in all copies of the 
 Software or substantial portions of the Software, and that both the above 
 copyright notice(s) and this license appear in supporting documentation.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE 
 COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE BE LIABLE 
 FOR ANY CLAIM, OR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL 
 DAMAGES, OR ANY DAMAGES WHATSOEVER (INCLUDING, BUT NOT 
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS 
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWSOEVER 
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN AN ACTION OF 
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
 OTHERWISE) ARISING IN ANY WAY OUT OF OR IN CONNECTION WITH THE 
 SOFTWARE OR THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE 
 POSSIBILITY OF SUCH DAMAGE.
 .
 Except as contained in this notice, the name of a copyright holder shall NOT
 be used in advertising or otherwise to promote the sale, use or other
 dealings in this Software without specific prior written authorization. 
 Title to copyright in this software and any associated documentation will at
 all times remain with copyright holders.

Files: debian/*
Copyright: 2012-2024 Mattias Ellert <mattias.ellert@physics.uu.se>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License"); you
 may not use this file except in compliance with the License. You may
 obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 implied.
 .
 On Debian systems the full text of the Apache-2.0 license can be
 found in the /usr/share/common-licenses/Apache-2.0 file.
